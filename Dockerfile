FROM eclipse-temurin:17-jdk-alpine

WORKDIR /app

COPY build/libs/videofile-1.jar ./application.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "application.jar"]
