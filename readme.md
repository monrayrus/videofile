## Тестовое задание для Java-разработчика

Описание задания указано в tech_description.md

Документация доступна после запуска приложения по url `http://localhost:8080/swagger-ui/index.html`

Для работы приложения необходимо, чтобы были установлены и запущены **Gradle** и **Docker**, а также исполняемые файлы **FFmpeg** и **FFprobe**. Скачать их можно здесь https://ffbinaries.com/downloads. Для работы с докером необходимы Linux версии

в **application.properties** необходимо определить пути к этим файлам

`ffmpeg.path=/volume/ffmpeg/ffmpeg`

`ffprobe.path=/volume/ffprobe/ffprobe`

`saving.path=/volume/savings/`

`output.path=/volume/savings/output/`

также в **docker-compose.yml** необходимо переопределить папки для внешних файлов `volumes`, по-умолчанию указан путь для windows систем по пути `D:\volume\ `

Чтобы запустить приложение, достаточно запустить startup.sh/bat в корневой папке проекта
