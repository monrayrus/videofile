#!/bin/bash

echo -e "\033[33mYou can check API endpoints after startup here http://localhost:8080/swagger-ui/index.html\033[0m"
echo -e "\033[33mRunning build process...\033[0m"
./gradlew clean
./gradlew test
./gradlew build
echo -e "\033[33mJAR build complete. Start Docker image build...\033[0m"
docker build -t videofile .
echo -e "\033[33mDocker image is built. Starting Docker Compose...\033[0m"
docker-compose up


