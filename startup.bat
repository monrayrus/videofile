@echo on
echo You can check API endpoints after startup here http://localhost:8080/swagger-ui/index.html
echo Running build process...
call gradlew clean
call gradlew test
call gradlew build
echo JAR build complete. Start Docker image build...
docker build -t videofile .
echo Docker image is built. Starting Docker Compose...
docker-compose up & echo Docker Compose completed. & echo You can check API endpoints here http://localhost:8080/swagger-ui/index.html
pause
