package ru.khusnullin.videofile;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import ru.khusnullin.videofile.controllers.VideoFileController;
import ru.khusnullin.videofile.services.VideoFileService;

import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VideoFileController.class)
public class VideoFileControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VideoFileService videoFileService;

    @Test
    void testFileUpload() throws Exception {
        MockMultipartFile file = new MockMultipartFile("file", "test.mp4", "video/mp4", "Spring Framework".getBytes());

        given(videoFileService.upload(file)).willReturn(UUID.randomUUID());

        mockMvc.perform(multipart("/file/").file(file))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists());
    }

    @Test
    void testFileDelete() throws Exception {
        UUID fileId = UUID.randomUUID();

        given(videoFileService.delete(fileId)).willReturn(true);

        mockMvc.perform(delete("/file/{id}/", fileId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true));
    }
}
