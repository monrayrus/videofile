package ru.khusnullin.videofile;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;
import ru.khusnullin.videofile.models.VideoFile;
import ru.khusnullin.videofile.repositories.VideoFileRepository;
import ru.khusnullin.videofile.services.VideoFileService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class VideoFileServiceTest {

    @Mock
    private VideoFileRepository repository;

    @InjectMocks
    private VideoFileService service;

    @Test
    public void testUpload() throws IOException {
        MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getOriginalFilename()).thenReturn("test.mp4");
        when(multipartFile.getBytes()).thenReturn("content".getBytes());

        UUID returnedId = service.upload(multipartFile);

        verify(repository, times(1)).save(any(VideoFile.class));
        assertNotNull(returnedId);

        String filePath = "null" + returnedId + ".mp4";

        boolean result = Files.deleteIfExists(Paths.get(filePath));
        if (!result) {
            System.out.println("Файл не удален.");
        }
    }

    @Test
    public void testDelete() {
        UUID id = UUID.randomUUID();
        VideoFile videoFile = new VideoFile();
        videoFile.setId(id);

        when(repository.findById(id)).thenReturn(Optional.of(videoFile));

        Boolean result = service.delete(id);

        verify(repository, times(1)).delete(any(VideoFile.class));
        assertTrue(result);
    }

}

