package ru.khusnullin.videofile.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.khusnullin.videofile.models.VideoFile;

import java.util.UUID;
@Repository
public interface VideoFileRepository extends JpaRepository<VideoFile, UUID> {
}
