package ru.khusnullin.videofile.services;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.khusnullin.videofile.models.VideoFile;
import ru.khusnullin.videofile.repositories.VideoFileRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.UUID;

@Service
public class VideoFileService {
    private final VideoFileRepository repository;
    private static final Logger log = LoggerFactory.getLogger(VideoFileService.class);
    @Value("${ffmpeg.path}")
    private String ffmpegPath;

    @Value("${ffprobe.path}")
    private String ffprobePath;

    @Value("${saving.path}")
    private String savingPath;

    @Value("${output.path}")
    private String outputPath;

    public VideoFileService(VideoFileRepository repository) {
        this.repository = repository;
    }

    public UUID upload(MultipartFile file) {
        UUID fileId = UUID.randomUUID();
        Path path = Paths.get(savingPath + fileId + ".mp4");
        try {
            Files.write(path, file.getBytes());
            log.info("File " + file.getOriginalFilename() + " uploaded with id: " + fileId);
        } catch (NullPointerException e) {
            log.info("No file found");
        } catch (IOException e) {
            log.error("Error uploading file: " + e.getMessage());
            throw new RuntimeException(e);
        }
        VideoFile videoFile = new VideoFile();
        videoFile.setId(fileId);
        videoFile.setFilename(file.getOriginalFilename());
        repository.save(videoFile);
        return fileId;
    }


    public Boolean delete(UUID id) {
        Optional<VideoFile> optionalVideoFile = repository.findById(id);
        boolean result = false;
        if (optionalVideoFile.isEmpty()) {
            log.warn("File with id " + id + " not found");
            return result;
        }
        repository.delete(optionalVideoFile.get());
        Path path = Paths.get(savingPath + id + ".mp4");
        try {
            Files.deleteIfExists(path);
            result = true;
            log.info("File with id " + id + " deleted");
        } catch (IOException e) {
            log.error("Error deleting file with id " + id + ": " + e.getMessage());
            throw new RuntimeException(e);
        }
        return result;
    }


    public Completable changeVideoResolution(UUID id, int width, int height) {
        return Completable.fromAction(() -> {
            VideoFile videoFile = repository.findById(id).orElseThrow(() -> new RuntimeException("Video file not found"));
            log.info("Getting videofile: " + videoFile.getFilename() + " : " + videoFile.getId());
            videoFile.setProcessing(true);
            repository.save(videoFile);
            log.info("Processing: " + videoFile.isProcessing() + ", ProcessingSuccess " + videoFile.isProcessingSuccess());

            try {
                FFmpeg ffmpeg = new FFmpeg(ffmpegPath);
                FFprobe ffprobe = new FFprobe(ffprobePath);

                FFmpegBuilder builder = new FFmpegBuilder()
                        .setInput(savingPath + id + ".mp4")
                        .overrideOutputFiles(true)
                        .addOutput(outputPath + id + "-" + width + "-" + height + ".mp4")
                        .setVideoResolution(width, height)
                        .done();

                FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
                executor.createJob(builder).run();

                videoFile.setProcessing(false);
                videoFile.setProcessingSuccess(true);
                log.info("In execution - Processing: " + videoFile.isProcessing() + ", ProcessingSuccess " + videoFile.isProcessingSuccess());
                repository.save(videoFile);
            } catch (Exception e) {
                videoFile.setProcessing(false);
                videoFile.setProcessingSuccess(false);
                log.info("Exception: " + e.getMessage() + "\nIn catch - Processing: " + videoFile.isProcessing() + ", ProcessingSuccess " + videoFile.isProcessingSuccess());
                repository.save(videoFile);
                throw e;
            }
        }).doOnComplete(() -> {
            log.info("Method changeVideoResolution completed for id: " + id);
        }).subscribeOn(Schedulers.io());
    }

    public VideoFile getFileStatus(UUID id) {
        Optional<VideoFile> optionalVideoFile = repository.findById(id);
        if (optionalVideoFile.isEmpty()) {
            log.warn("File with id " + id + " not found");
            throw new RuntimeException("Video file not found");
        }
        return optionalVideoFile.get();
    }

}
