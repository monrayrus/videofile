package ru.khusnullin.videofile.controllers;

import io.reactivex.rxjava3.schedulers.Schedulers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.khusnullin.videofile.models.VideoFile;
import ru.khusnullin.videofile.services.VideoFileService;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@Tag(name = "Video File Management", description = "Operations related to video files")
public class VideoFileController {
    private final VideoFileService service;
    private static final Logger log = LoggerFactory.getLogger(VideoFileController.class);

    public VideoFileController(VideoFileService service) {
        this.service = service;
    }

    @Operation(summary = "Upload a new video file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Video file uploaded successfully",
                    content = @Content(schema = @Schema(example = "{\"id\":\"e38e8fbf-ab1b-475c-8b03-0218c4788fc9\"}"))),
            @ApiResponse(responseCode = "500", description = "Invalid request data",
                    content = @Content(schema = @Schema(example = "{\n" + "\"error\": \"Error description\"\n" + "}")))
    })
    @PostMapping(value = "/file/", consumes = "multipart/form-data")
    public ResponseEntity<?> upload(@Parameter(description = "The file to upload") @RequestPart("file") MultipartFile file) throws IOException {
        UUID id = service.upload(file);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("id", id);
        return ResponseEntity.ok().body(body);
    }

    @Operation(summary = "Delete a video file by its ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Video file deleted successfully"),

            @ApiResponse(responseCode = "500", description = "Invalid request data",
                    content = @Content(schema = @Schema(example = "{\n" + "\"error\": \"Error description\"\n" + "}")))
    })
    @DeleteMapping("/file/{id}/")
    public ResponseEntity<?> delete(@PathVariable UUID id) {
        Boolean success = service.delete(id);
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("success", success);
        return ResponseEntity.ok().body(body);
    }

    @Operation(summary = "Change the resolution of a video file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Process of changing resolution has started successfully",
                    content = @Content(schema = @Schema(example = "{\"success\":\"true\"}"))),
            @ApiResponse(responseCode = "500", description = "Invalid request data",
                    content = @Content(schema = @Schema(example = "{\n" + "\"error\": \"Error description\"\n" + "}")))
    })
    @PatchMapping("/file/{id}/")
    public ResponseEntity<?> changeResolution(@PathVariable("id") UUID id,
                                              @RequestBody(required = true) @Schema(example = "{\"width\":\"10\", \"height\":\"5\"}") Map<String, Integer> body) {
        log.info("ID: " + id);
        int width = body.get("width");
        int height = body.get("height");
        log.info("Got width: " + width + ", and height: " + height);

        service.changeVideoResolution(id, width, height)
                .subscribeOn(Schedulers.io())
                .subscribe(
                        () -> {
                        },
                        throwable -> {
                        }
                );
        log.info("Returning result...");

        return ResponseEntity.ok(Map.of("success", true));
    }

    @Operation(summary = "Retrieve the status of a video file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Video file status retrieved successfully",
                    content = @Content(schema = @Schema(example = "{\"success\":\"true\"}"))),
            @ApiResponse(responseCode = "404", description = "Video file not found",
                    content = @Content(schema = @Schema(example = "{\n" + "\"error\": \"Error description\"\n" + "}")))
    })
    @GetMapping("/file/{id}/")
    public ResponseEntity<VideoFile> getFileStatus(@PathVariable UUID id) {
        VideoFile videoFile = service.getFileStatus(id);
        return ResponseEntity.ok(videoFile);
    }


}



