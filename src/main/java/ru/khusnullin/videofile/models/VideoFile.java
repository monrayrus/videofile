package ru.khusnullin.videofile.models;

import jakarta.annotation.Nullable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

import java.util.UUID;

@Entity
public class VideoFile {

    @Id
    private UUID id;
    private String filename;
    private boolean processing;
    @Nullable
    private Boolean processingSuccess;

    public VideoFile() {
    }

    public VideoFile(String filename, boolean processing, boolean processingSuccess) {
        this.filename = filename;
        this.processing = processing;
        this.processingSuccess = processingSuccess;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    public Boolean isProcessingSuccess() {
        return processingSuccess;
    }

    public void setProcessingSuccess(Boolean processingSuccess) {
        this.processingSuccess = processingSuccess;
    }

}
