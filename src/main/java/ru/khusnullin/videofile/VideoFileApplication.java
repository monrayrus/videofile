package ru.khusnullin.videofile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VideoFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(VideoFileApplication.class, args);
	}

}
